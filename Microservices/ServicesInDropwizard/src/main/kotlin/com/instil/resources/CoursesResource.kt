package com.instil.resources

import java.util.*
import javax.ws.rs.*

import com.instil.model.Course

import javax.ws.rs.core.MediaType.*

@Path("/courses")
class CoursesResource(private val courses: MutableMap<String, Course>) {
    @GET
    @Produces(APPLICATION_JSON)
    fun allCoursesAsJson(): List<Course> {
        return ArrayList(courses.values)
    }

    @GET
    @Path("{id}")
    @Produces(APPLICATION_JSON)
    fun courseByIdAsJson(@PathParam("id") id: String): Course? {
        return courses[id]
    }

    @DELETE
    @Path("{id}")
    @Produces(TEXT_PLAIN)
    fun deleteACourse(@PathParam("id") id: String): String {
        courses.remove(id)
        return id
    }

    @PUT
    @Path("{id}")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    fun addOrUpdateCourseFromJson(@PathParam("id") id: String, newCourse: Course): Course {
        courses[newCourse.id] = newCourse
        return newCourse
    }
}
