package com.instil

import com.instil.resources.CoursesResource
import com.instil.model.Builder
import io.dropwizard.Application
import io.dropwizard.setup.Bootstrap
import io.dropwizard.setup.Environment

/*
    Run with the following:
    java -jar build/libs/ServicesInDropwizard-1.0-SNAPSHOT-all.jar server config.yaml
 */
class DemoApplication : Application<DemoConfiguration>() {

    override fun initialize(bootstrap: Bootstrap<DemoConfiguration>) {
    }

    override fun run(
        configuration: DemoConfiguration,
        environment: Environment) {
        println("Running service for ${configuration.clientName}")
        val resource = CoursesResource(Builder.buildPortfolio())
        environment.jersey().register(resource)
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            DemoApplication().run(*args)
        }
    }
}
