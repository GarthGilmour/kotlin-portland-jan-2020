package com.instil.controllers

import com.instil.model.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import java.util.*
import kotlin.system.measureTimeMillis


//This controller will be synchronous - not reactive
@RestController
@RequestMapping("/courses-jpa")
class CourseControllerJPA(@Autowired val courseService: CourseRepository,
                          @Autowired val demoCourses: List<Course>) {
    @GetMapping
    fun all(): MutableIterable<Course> {
        var findAll: MutableIterable<Course> = mutableListOf()
        val time = measureTimeMillis {
             findAll = courseService.findAll()
        }
        println("Time to get all = $time ms")
        return findAll
    }

    @DeleteMapping("/{id}")
    fun delete(@PathVariable("id") id: String) {
        return courseService.deleteById(id)
    }

    @GetMapping("/{id}")
    fun getById(@PathVariable("id") id: String): Optional<Course> {
        return courseService.findById(id)
    }

    @GetMapping("/byKeyword/{search}")
    fun getByKeyword(@PathVariable("search") search: String): Iterable<Course> {
        return courseService.findByTitleIgnoreCase(search)
    }

    @PostMapping("/reset")
    fun reset() {
        courseService.deleteAll()
        courseService.saveAll(demoCourses)
    }
}