package com.instil;


import org.reactivestreams.Publisher;
import reactor.core.publisher.Mono;
import reactor.netty.http.server.HttpServer;
import reactor.netty.http.server.HttpServerRequest;
import reactor.netty.http.server.HttpServerResponse;

import java.util.Scanner;
import java.util.function.BiFunction;

public class Program {
    private static final int PORT = 8080;
    private static final String ENDPOINT = "/my-endpoint";
    private static final String MSG = "Hello Reactor Netty";

    public static void main(String[] args) {
        BiFunction<HttpServerRequest, HttpServerResponse, Publisher<Void>> func;
        func = (request, response) -> response.sendString(Mono.just(MSG));

        HttpServer.create()
                .port(PORT)
                .route(routes -> routes.get(ENDPOINT, func))
                .bindNow();

        System.out.println("Hit return to exit");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
}
