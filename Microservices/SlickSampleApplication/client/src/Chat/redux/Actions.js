import {createAction, createAsyncAction, createEmptyAction} from "../../Shared/Utils/ActionCreators";
import {config} from "../../config";
import {extractMessage} from "../../Shared/Utils/MessageParser";
import {WebSocketPromises} from "../../Shared/Utils/WebSocketPromises";
import {ChatService} from "../services/ChatService";
import {Log} from "../../Shared/Utils/Logger"
import {remapIM, remapIMs} from "../model/IM";

export const setRoomName = createAction('SET_ROOM_NAME');
export const changingRoom = createEmptyAction('CHANGING_ROOM');
export const setRoomUserCount = createAction('SET_ROOM_USER_COUNT');
export const setChatSocket = createAction('SET_CHAT_SOCKET');
export const newIM = createAction('NEW_IM');
export const chatLoad = createAction('CHAT_LOAD');
export const currentMessageUpdate = createAction('MESSAGE_UPDATE');
export const sendNewIM = createAsyncAction('SEND_IM');

function processMessage(dispatch, message) {
    switch (message.id) {
        case 'ALL_IMS':
            dispatch(chatLoad(remapIMs(message.messages)));
            break;
        case 'NEW_IM':
            dispatch(newIM(remapIM(message.message)));
            break;
        case "USER_COUNT":
            dispatch(setRoomUserCount(message.count));
            break;
        default:
            Log.error(`Unknown message received: ${message.id}`);
    }
}

async function clearOldSocket(getState) {
    const oldSocket = getState().chat.socket;
    if (oldSocket) {
        await oldSocket.close();
    }
}

export const selectRoom = name => async (dispatch, getState) => {
    dispatch(changingRoom());
    console.log('Clearing socket');
    await clearOldSocket(getState);
    console.log('Socket cleared');

    dispatch(setRoomName(name));

    if (name !== '') {
        const socket = new WebSocketPromises(`${config.wsPrefix}${config.chatServer}/chat/messages/${name}`);

        socket.addMessageHandler(extractMessage(message => processMessage(dispatch, message)));

        await socket.waitUntilOpen();
        Log.debug(`Chat ${name} Socket connection open`);
        dispatch(setChatSocket(socket));
    }
};

export const sendIMAsync = (room, message) => async (dispatch) => {
    dispatch(sendNewIM.request());

    try {
        await ChatService.sendIM(room, message);
        dispatch(sendNewIM.success(room));
    } catch (error) {
        dispatch(sendNewIM.failure(error));
    }
};
