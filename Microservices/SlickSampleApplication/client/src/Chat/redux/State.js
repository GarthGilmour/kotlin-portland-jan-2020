
export const initialState = {
    name: null,
    socket: null,
    messages: [],
    isSending: false,
    currentMessages: {
    },
    userCount: 0,
    changingRoom: false
};
