import axios from "axios";
import {config} from "../../config";

export class ChatService {
    static async sendIM(room, message) {
        const response = await axios.post(`${config.httpPrefix}${config.chatServer}/chat/messages/${room}`, message);
        if (response.status !== 200) {
            throw new Error("Failed to post IM");
        }
    }
}
