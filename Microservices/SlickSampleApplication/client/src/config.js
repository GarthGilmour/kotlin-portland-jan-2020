
// INTERESTING - create-react-app supports COMPILE time
//               environment variables
const environment = process.env.REACT_APP_ENV.trim();

const prodConfig = {
    wsPrefix: 'ws://',
    httpPrefix: 'http://',
    userServer: 'localhost/api',
    chatServer: 'localhost/api'
};

const devConfig = {
    wsPrefix: 'ws://',
    httpPrefix: 'http://',
    userServer: 'localhost:8080/api',
    chatServer: 'localhost:8080/api'
};

export const config = environment === 'prod'
    ? prodConfig
    : devConfig;

console.log(`Environment: [${environment}]`);
console.log(`User Server: ${config.userServer}`);
console.log(`Chat Server: ${config.chatServer}`);
