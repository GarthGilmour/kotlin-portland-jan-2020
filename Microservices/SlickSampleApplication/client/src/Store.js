import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import {coreReducer} from "./Core/redux/Reducer";
import {userReducer} from "./User/redux/Reducer";
import thunk from "redux-thunk";
import {chatReducer} from "./Chat/redux/Reducer";
import {initialiseUser} from "./User/redux/Actions";

const reducer = combineReducers({
    core: coreReducer,
    user: userReducer,
    chat: chatReducer
});

// INTERESTING - Redux Dev Tools give nice debugging in the Browser
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(reducer,
    composeEnhancers(applyMiddleware(thunk)));

store.dispatch(initialiseUser());
