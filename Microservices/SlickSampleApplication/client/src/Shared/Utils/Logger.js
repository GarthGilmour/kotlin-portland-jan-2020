
const debug = true; // process.env.NODE_ENV !== 'production'

export class Log {
    static debug = debug ? console.log : () => {};
    static error = console.log;
}

