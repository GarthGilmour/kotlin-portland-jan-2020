

export const extractMessage = (func) => (event) => {
    const message = JSON.parse(event.data);
    return func(message);
};
