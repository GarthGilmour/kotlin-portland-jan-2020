import {Log} from "./Logger";

export class WebSocketPromises {
    constructor(url) {
        this.socket = new WebSocket(url);
        this.messageHandlers = [];

        this._openPromise = new Promise((resolve) => {
            this.socket.onopen = (event) => resolve(event);
        });

        this._closePromise = new Promise((resolve) => {
            this.socket.onclose = (event) => resolve(event);
        });

        this.socket.onerror = (event) => {
            Log.error('Socket Error');
            Log.error(event);
        };
    }

    addMessageHandler(handler) {
        this.socket.addEventListener('message', handler);
        this.messageHandlers.push(handler);
    }

    async waitUntilOpen() {
        await this._openPromise;
    }

    async close() {
        this.messageHandlers.forEach(x => this.socket.removeEventListener('message', x));
        this.socket.close();
        await this._closePromise;
    }
}
