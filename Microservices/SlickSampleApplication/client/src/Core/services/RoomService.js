import axios from "axios";
import {config} from "../../config";

export class RoomService {
    static async add(name) {
        const response = await axios.post(`${config.httpPrefix}${config.chatServer}/chat/room/${name}`);
        if (response.status !== 200) {
            throw new Error("Failed to add room");
        }
    }
}
