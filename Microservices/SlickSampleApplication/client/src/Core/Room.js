import React, {useState} from "react";
import styles from "./Room.module.scss";
import {selectRoom} from "../Chat/redux/Actions";
import {addRoom} from "../Core/redux/Actions";
import {connect} from "react-redux";

const AddRooms = ({addRoom, disabled}) => {
    const [newRoom, setNewRoom] = useState('');
    const addRoomHandler = (e) => {
       addRoom(newRoom);
       setNewRoom('');
    };

    return (
        <div className='row ml-2 mr-2'>
            <input className='col-12' type='text'
                   value={newRoom} onChange={e => setNewRoom(e.target.value)}/>
            <button className='col-12 btn btn-primary'
                    onClick={addRoomHandler}
                    disabled={newRoom.trim() === '' || disabled}>
                Add Room
            </button>
        </div>
    );
};

const NavButton = React.memo(({title, selected, onSelect, disabled}) => {
    const buttonStyle = selected ? 'btn-primary' : 'btn-outline-primary';
    return (
        <div>
            <button disabled={disabled} className={`${styles.navButton} btn ${buttonStyle} mb-2`}
                    onClick={() => onSelect(title)}>
                {title}
            </button>
        </div>
    );
});

const Rooms = ({rooms, currentRoom, isDisabled, selectRoom, addRoom}) => (
    <div>
        <div>
            {rooms.map(({name}) => <NavButton key={name} title={name}
                                              disabled={isDisabled}
                                              selected={name === currentRoom}
                                              onSelect={selectRoom}/>)
            }
        </div>
        <div className={`mb-1 ${styles.separator}`}/>
        <div>
            <AddRooms addRoom={addRoom} disabled={isDisabled}/>
        </div>
    </div>
);

const mapStateToProps = (state) => ({
    rooms: state.core.rooms,
    currentRoom: state.chat.name,
    isDisabled: state.chat.changingRoom
});

const mapDisaptchToProps = ({
    selectRoom,
    addRoom
});

export default connect(mapStateToProps, mapDisaptchToProps)(Rooms);
