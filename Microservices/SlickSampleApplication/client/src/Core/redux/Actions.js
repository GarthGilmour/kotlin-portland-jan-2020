import {createAction, createEmptyAction} from "../../Shared/Utils/ActionCreators";
import {config} from "../../config";
import {extractMessage} from "../../Shared/Utils/MessageParser";
import {selectRoom} from "../../Chat/redux/Actions";
import {Log} from "../../Shared/Utils/Logger";
import {RoomService} from "../services/RoomService";

export const roomUpdate = createAction('ROOM_UPDATE');
export const roomsLoad = createAction('ROOM_LOAD');
export const roomsSocketOpened = createEmptyAction('ROOM_SOCKET_OPENED');
export const roomSocketClosed = createEmptyAction('ROOM_SOCKET_CLOSED');
export const serverFailure = createEmptyAction('SERVER_FAILURE');

const MAX_CONNECTION_FAILS = 5;

function processMessage(dispatch, message, getState) {
    switch (message.id) {
        case 'ALL_ROOMS':
            dispatch(roomsLoad(message.rooms));
            break;
        case 'ROOM_UPDATE':
            const name = getState().chat.name;
            let {type, room} = message.update;
            if (type === 'ADD' && (!name || name === '')) {
                dispatch(selectRoom(room.name));
            } else if (type === 'REMOVE' && name === room.name) {
                dispatch(selectRoom(''));
            }
            dispatch(roomUpdate(message.update));
            break;
        default:
            Log.error(`Unknown message received${message.id}`);
    }
}

export function connectToRoomsSocket() {
    return (dispatch, getState) => {
        const socket = new WebSocket(`${config.wsPrefix}${config.chatServer}/chat/room`);
        socket.onopen = (event) => {
            Log.debug('Socket connection open: ');
            Log.debug(event);
            dispatch(roomsSocketOpened());
        };
        socket.onclose = () => {
            Log.debug('Socket connection closed');

            if (getState().core.connectionFails === MAX_CONNECTION_FAILS) {
                dispatch(serverFailure());
            } else {
                dispatch(roomSocketClosed());
                dispatch(connectToRoomsSocket());
                dispatch(selectRoom(getState().chat.name))
            }
        };

        socket.onmessage = extractMessage(message => processMessage(dispatch, message, getState));

        socket.onerror = (event) => {
            Log.error('Rooms Socket Error');
            Log.error(event);
        };
    };
}

export const addRoom = name => async (dispatch) => {
    try {
        await RoomService.add(name);
    } catch (error) {
        Log.error(error);
    }
};
