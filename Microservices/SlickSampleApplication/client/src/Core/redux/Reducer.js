import {initialState} from "./State";
import {roomsLoad, roomSocketClosed, roomUpdate, serverFailure, roomsSocketOpened} from "./Actions";

export const coreReducer = (state = initialState, action) => {
    switch (action.type) {
        case roomsLoad.id:
            return {
                ...state,
                rooms: action.payload
            };
        case roomUpdate.id:
            let {type, room} = action.payload;
            if (type === 'ADD') {
                return {
                    ...state,
                    rooms: [...state.rooms, room]
                }
            } else if (type === 'REMOVE') {
                return {
                    ...state,
                    rooms: state.rooms.filter(x => x.name !== room.name)
                }
            } else {
                return state
            }
        case roomSocketClosed.id:
            return {
                ...state,
                rooms: [],
                connectionFails: state.connectionFails + 1,
                connectingToServer: true
            };
        case roomsSocketOpened.id:
            return {
                ...state,
                rooms: [],
                connectionFails: 0,
                connectingToServer: false
            };
        case serverFailure.id:
            return {
                ...state,
                serverFailure: true
            };
        default:
            return state;
    }
};
