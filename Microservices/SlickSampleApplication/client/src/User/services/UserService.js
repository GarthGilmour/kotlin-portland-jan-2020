import axios from "axios";
import {config} from "../../config";

export class UserService {
    static async login(username, password) {
        const response = await axios.post(`${config.httpPrefix}${config.userServer}/authentication/login`,
            {username, password}
        );

        if (response.status !== 200) {
            throw Error(response.data);
        }

        return response.data;
    }

    static async currentuser() {
        const response = await axios.get(`${config.httpPrefix}${config.userServer}/authentication`);
        if (response.status !== 200) {
            throw Error("'Not logged in");
        }

        return response.data
    }
}
