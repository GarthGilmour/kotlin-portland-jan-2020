import React from 'react';
import {connect} from "react-redux";
import {updateLoginField, loginAsync} from "./redux/Actions";

export const Login = ({isLoggingIn, loginAsync, loginValid, username, password, updateField}) => (
    <div className="bg-dark p-3">
        <h2>Login</h2>
        <div className='form-group '>
            <label htmlFor="username">Username</label>
            <input type="text" className="form-control" name="username" value={username}
                   onChange={(e) => updateField({field: 'username', text: e.target.value})}/>
        </div>
        <div className='form-group'>
            <label htmlFor="password">Password</label>
            <input type="password" className="form-control" name="password" value={password}
                   onChange={(e) => updateField({field: 'password', text: e.target.value})}/>
        </div>
        <div className="form-group">
            <button className="btn btn-primary" disabled={isLoggingIn || !loginValid}
                    onClick={() => loginAsync(username, password)}>Login
            </button>
        </div>
    </div>
);

const mapStateToProps = (state) => ({
    isLoggingIn: state.user.isLoggingIn,
    username: state.user.login.username,
    password: state.user.login.password,
    currentUser: state.user.currentUser,
    loginValid: state.user.login.username && state.user.login.password
});

const mapDisaptchToProps = ({
    updateField: updateLoginField,
    loginAsync
});

export default connect(mapStateToProps, mapDisaptchToProps)(Login);
