import {initialState} from "./State";
import {updateLoginField, login, logout, initialise} from "./Actions";

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case updateLoginField.id:
            return {
                ...state,
                login: {
                    ...state.login,
                    [action.payload.field]: action.payload.text
                }
            };
        case login.request.id:
            return {
                ...state,
                isLoggingIn: true
            };
        case login.success.id:
            return {
                ...state,
                isLoggingIn: false,
                error: '',
                login: {
                    ...state.login,
                    password: ''
                },
                currentUser: action.payload
            };
        case login.failure.id:
            return {
                ...state,
                isLoggingIn: false,
                error: action.payload,
            };
        case logout.id:
            return {
                ...state,
                currentUser: null,
                isLoggingIn: false,
                login: {
                    ...state.login,
                    password: ''
                },
                error: action.payload,
            };
        case initialise.id:
            return {
                ...state,
                isInitialised: true
            };
        default:
            return state;
    }
};
