import {createAction, createAsyncAction} from "../../Shared/Utils/ActionCreators";
import {UserService} from "../services/UserService";
import {JwtService} from "../services/JwtService";
import {connectToRoomsSocket} from "../../Core/redux/Actions";
import {Log} from "../../Shared/Utils/Logger";

export const login = createAsyncAction('LOGIN');
export const logout = createAction('LOGOUT');
export const initialise = createAction('INITIALISE');
export const updateLoginField = createAction('UPDATE_LOGIN_FIELD');

export const loginAsync = (username, password) => async (dispatch) => {
    dispatch(login.request());

    try {
        const response = await UserService.login(username, password);
        JwtService.set(response.token);
        dispatch(login.success(response.user));
        dispatch(connectToRoomsSocket());
    } catch (error) {
        Log.debug(error);
        dispatch(login.failure("Login failed"));
    }
};

export const logoutUser = () => (dispatch) => {
    JwtService.clear();
    dispatch(logout());
};

export const initialiseUser = () => async (dispatch) => {
    if (JwtService.get()) {
        try {
            const user = await UserService.currentuser();
            dispatch(login.success(user));
            dispatch(connectToRoomsSocket());
        } catch (error) {
            JwtService.clear();
        }
    }

    dispatch(initialise());
};

