# Example
In this example we are going to learn

- Tools and Setup
- React
- Kotlin
- Ktor

# Setup

## TL;DR

Prerequisites
- Node (at least 10.15)
- yarn 
  - ```npm install -g yarn```
- Java 8
- IntelliJ

You can use an IDE to run the projects or via the command line

Front End
```
$ cd client/slick
$ yarn install
$ yarn start
```

Back End
```
$ cd server
$ ./gradlew run
```

Use the test routes in 'server/test/routes' to add some rooms.


# Microsoft Windows Considerations

The build scripts are bash scripts. Use 'Git Bash' to simplify execution.

'cross-env' is being used in the client 'package.json' so that
we can set environment variables within scripts and it will
work with Windows and Mac.

