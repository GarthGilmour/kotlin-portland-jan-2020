package co.instil.slick.chat

import co.instil.slick.shared.User
import co.instil.slick.room.Room
import co.instil.slick.utils.ISubject
import java.io.Closeable

data class NewChatMessage(val room: Room, val message: ChatIM)

interface ChatService : ISubject<NewChatMessage>, Closeable {
    suspend fun getChat(room: Room): List<ChatIM>

    suspend fun addMessage(room: Room, user: User, content: String)
}

val chatService = ChatServiceInMemory()