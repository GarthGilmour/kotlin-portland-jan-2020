package co.instil.slick.chat

import co.instil.slick.shared.SlickUserPrincipal
import co.instil.slick.shared.User
import co.instil.slick.room.Room
import co.instil.slick.utils.*
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.websocket.DefaultWebSocketServerSession
import io.ktor.websocket.webSocket
import org.slf4j.LoggerFactory
import java.util.Collections.synchronizedMap

val chatSockets: MutableMap<Room, SocketHandler> = synchronizedMap(mutableMapOf<Room, SocketHandler>())

private val logger = LoggerFactory.getLogger("co.instil.slick.Chat")

// INTERESTING - Extension methods for routes
fun Route.chatRoutes() {
    chatService.subscribe {
        logger.debug("Broadcasting new mesage ${it.message}")
        chatSockets[it.room]?.broadcast(ChatMessage.NewIM(it.message).toJsonString())
    }

    // INTERESTING - Parameters on route
    route("messages/{roomname}") {
        // TODO: Authentication on Web Socket
        webSocket {
            val roomName = call.parameters["roomname"] ?: error("Missing Room name parameter")
            logger.debug("Joining room $roomName")
            val room = Room(roomName)

            // INTERESTING - There is a still a race condition here
            val socketHandler = chatSockets.getOrPut(room, { SocketHandler() })
            socketHandler.add(this)
            notifyUsersInRoom(room)
            try {
                sendChat(room)
                waitUntilClosed()
            } finally {
                logger.debug("Leaving room $roomName")
                socketHandler.remove(this)
                notifyUsersInRoom(room)
            }
        }

        authenticate {
            get {
                // INTERESTING - Extracting parameters on route
                val roomName = call.parameters["roomname"] ?: error("Missing Room name parameter")
                call.respond(mapOf("messages" to chatService.getChat(Room(roomName)).toListSynchronised()))
            }

            post {
                val roomName = call.parameters["roomname"]
                val user = call.principal<SlickUserPrincipal>()
                if (roomName == null || user == null) {
                    call.respond(HttpStatusCode.BadRequest)
                    return@post
                }

                chatService.addMessage(Room(roomName), User(user.username, "", user.id), call.receiveText())
                call.respondOk()
            }
        }
    }
}

suspend fun notifyUsersInRoom(room: Room) {
    chatSockets[room]?.let {
        it.broadcast(ChatMessage.UserCount(it.size).toJsonString())
    }
}

private suspend fun DefaultWebSocketServerSession.sendChat(room: Room) {
    val chat = chatService.getChat(room)

    outgoing.send(ChatMessage.AllIMs(chat).toJsonFrame())
}