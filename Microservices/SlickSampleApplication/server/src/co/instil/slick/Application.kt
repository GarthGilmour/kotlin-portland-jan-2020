package co.instil.slick

import co.instil.slick.authentication.authenticationRoutes
import co.instil.slick.chat.chatRoutes
import co.instil.slick.room.roomRoutes
import com.fasterxml.jackson.databind.SerializationFeature
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.http.ContentType
import io.ktor.jackson.jackson
import io.ktor.request.path
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import io.ktor.websocket.WebSockets
import org.slf4j.LoggerFactory
import java.time.Duration

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

private val logger = LoggerFactory.getLogger("co.instil.slick.Application")

@KtorExperimentalAPI
@Suppress("unused") // Referenced in application.conf
fun Application.module() {
    installWebSockets()
    installAuthentication()
    installJSONSerialisation()
    installCORS()

    routing {
        trace {
            logger.debug("Request uri: " + it.call.request.path())
        }

        route("api") {
            get("/") {
                call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
            }

            route("authentication") {
                authenticationRoutes()
            }

            route("chat") {
                chatRoutes()
                roomRoutes()
            }
        }
    }
}

fun Application.installCORS() {
    install(CORS) {
        anyHost()
        header("Authorization")
    }
}

private fun Application.installJSONSerialisation() {
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }
}

private fun Application.installWebSockets() {
    install(WebSockets) {
        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }
}



