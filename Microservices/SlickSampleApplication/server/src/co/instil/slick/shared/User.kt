package co.instil.slick.shared

import java.util.*

data class User(val name: String, val password: String, val id: UUID = UUID.randomUUID())

data class SlickUserPrincipal(val username: String, val id: UUID) : io.ktor.auth.Principal

