package co.instil.slick.room

import co.instil.slick.utils.ISubject

@Suppress("unused")
sealed class RoomUpdate(val type: String, val room: Room) {
    class Add(room: Room) : RoomUpdate("ADD", room)
    class Remove(room: Room) : RoomUpdate("REMOVE", room)
}

interface RoomService : ISubject<RoomUpdate> {
    val rooms: Iterable<Room>

    suspend fun add(name: String)

    suspend fun remove(name: String)
}


val roomService: RoomService = RoomServiceInMemory()


