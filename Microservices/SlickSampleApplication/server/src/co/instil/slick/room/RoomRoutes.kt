package co.instil.slick.room

import co.instil.slick.utils.*
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.websocket.DefaultWebSocketServerSession
import io.ktor.websocket.webSocket
import org.slf4j.LoggerFactory

val allRoomsSockets = SocketHandler()

private val logger = LoggerFactory.getLogger("co.instil.Room")

fun Route.roomRoutes() {
    roomService.subscribe {
        allRoomsSockets.broadcast(RoomMessage.RoomUpdate(it).toJsonString())
    }

    route("room") {
        webSocket {
            allRoomsSockets.add(this)
            try {
                sendRooms()
                waitUntilClosed()
            } finally {
                allRoomsSockets.remove(this)
            }
        }

        authenticate {
            get {
                call.respond(mapOf("rooms" to roomService.rooms))
            }

            route("{roomName}") {
                post {
                    val name = call.parameters["roomName"] ?: error("Missing room name parameter")
                    logger.info("Adding Room $name")
                    roomService.add(name)
                    call.respondOk()
                }

                delete {
                    val name = call.parameters["roomName"] ?: error("Missing room name parameter")
                    logger.info("Removing Room $name")
                    roomService.remove(name)
                    call.respondOk()
                }
            }
        }
    }
}

private suspend fun DefaultWebSocketServerSession.sendRooms() {
    sendJson(RoomMessage.AllRooms(roomService.rooms))
}