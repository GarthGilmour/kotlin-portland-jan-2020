package co.instil.slick.room

import co.instil.slick.utils.Subject
import co.instil.slick.utils.valuesListSynchronised
import java.util.*

@Suppress("unused")
class RoomServiceInMemory : RoomService, Subject<RoomUpdate>() {
    private val _rooms = Collections.synchronizedMap(
            listOf(
                    Room("general"),
                    Room("kotlin"),
                    Room("movies")
            )
                    .associateBy { it.name }
                    .toMutableMap()
    )

    override suspend fun add(name: String) {
        val room = Room(name)
        synchronized(_rooms) {
            if (_rooms.containsKey(name)) {
                return
            }

            _rooms[name] = room
        }
        invokeCallbacks(RoomUpdate.Add(room))
    }

    override suspend fun remove(name: String) {
        val room = _rooms.remove(name)
        if (room != null) {
            invokeCallbacks(RoomUpdate.Remove(room))
        }
    }

    // INTERESTING - Computed Properties
    override val rooms: Iterable<Room>
        get() = _rooms.valuesListSynchronised()
}