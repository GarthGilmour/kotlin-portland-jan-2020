package co.instil.slick.authentication

import co.instil.slick.shared.User
import java.util.*
import java.util.Collections.synchronizedMap

class UserServiceInMemory {
    private val users = synchronizedMap(
            listOf(
                User("user1", "password1"),
                User("user2", "password2")
            )
                    .associateBy { it.id }
                    .toMutableMap()
    )

    fun add(user: User) = users.set(user.id, user)
    fun get(id: UUID) = users[id]
    fun getAll() = users.values.toList()
    fun getByName(username: String) = users.values.find { it.name == username }
}

val userService = UserServiceInMemory()
