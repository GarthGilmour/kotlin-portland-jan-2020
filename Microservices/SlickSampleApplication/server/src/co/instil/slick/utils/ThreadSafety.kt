package co.instil.slick.utils

fun <T> Iterable<T>.toListSynchronised() =
    synchronized(this) { this.toList() }

fun <K, V> Map<K, V>.valuesListSynchronised() =
    synchronized(this) { this.values.toList() }
