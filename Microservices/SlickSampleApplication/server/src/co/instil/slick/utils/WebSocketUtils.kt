package co.instil.slick.utils

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.ktor.http.cio.websocket.Frame
import io.ktor.websocket.DefaultWebSocketServerSession

val mapper = jacksonObjectMapper()

suspend fun DefaultWebSocketServerSession.sendJson(value: Any) =
        send(value.toJsonFrame())

// INTERESTING - Extensions
fun Any.toJsonFrame() = Frame.Text(this.toJsonString())

fun Any.toJsonString(): String = mapper.writeValueAsString(this)

// INTERESTING - Reified Generics on the JVM
inline fun <reified T> String.fromJsonString(): T =
        mapper.readValue(this, T::class.java)

suspend fun DefaultWebSocketServerSession.waitUntilClosed() {
    for (frame in incoming) {
        // Don't do anything just tracks the lifetime of the socket
    }
}
