package co.instil.slick.utils

import io.ktor.http.cio.websocket.Frame
import io.ktor.websocket.DefaultWebSocketServerSession
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import org.slf4j.LoggerFactory
import java.util.Collections.synchronizedList

private val logger = LoggerFactory.getLogger(SocketHandler::class.java)

class SocketHandler : CoroutineScope by CoroutineScope(Dispatchers.IO) {
    private val sockets: MutableList<DefaultWebSocketServerSession> =
            synchronizedList(mutableListOf<DefaultWebSocketServerSession>())

    fun remove(socket: DefaultWebSocketServerSession) {
        logger.debug("Removing socket")
        sockets.remove(socket)
    }

    fun add(socket: DefaultWebSocketServerSession) {
        logger.debug("Adding socket")
        sockets.add(socket)
    }

    val size: Int
        get() = sockets.size

    suspend fun broadcast(content: String) {
        val sockets = sockets.toListSynchronised()
        if (sockets.isNotEmpty()) logger.debug("Broadcasting to ${sockets.size} clients")

        sockets.forEach {
            it.outgoing.send(Frame.Text(content))
        }
    }
}