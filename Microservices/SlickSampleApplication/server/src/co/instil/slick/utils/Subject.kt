package co.instil.slick.utils

import java.util.Collections.synchronizedList

interface ISubject<T> {
    fun subscribe(callback: suspend (T) -> Unit)
}

open class Subject<T> : ISubject<T> {
    private val updateCallbacks = synchronizedList(
            mutableListOf<suspend (T) -> Unit>())

    override fun subscribe(callback: suspend (T) -> Unit) {
        updateCallbacks.add(callback)
    }

    protected suspend fun invokeCallbacks(update: T) {
        updateCallbacks.toListSynchronised().forEach { it(update) }
    }
}