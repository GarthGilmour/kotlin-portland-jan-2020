package co.instil.slick

import co.instil.slick.shared.SlickUserPrincipal
import co.instil.slick.shared.jwtService
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt
import io.ktor.util.KtorExperimentalAPI
import org.slf4j.LoggerFactory
import java.util.*

private val logger = LoggerFactory.getLogger("co.instil.slick.Authentication")

@KtorExperimentalAPI
fun Application.installAuthentication() {
    val jwtRealm = environment.config.property("jwt.realm").getString()

    install(Authentication) {
        jwt {
            realm = jwtRealm
            verifier(jwtService.verifier)
            validate {
                val name = it.payload.getClaim("username").asString()
                val id = it.payload.getClaim("id").asString()
                logger.debug("Request validation for : $name")
                SlickUserPrincipal(name, UUID.fromString(id))
            }
        }
    }
}