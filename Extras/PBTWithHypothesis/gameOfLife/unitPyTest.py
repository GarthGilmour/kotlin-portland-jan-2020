from gameOfLife.cell import Cell
import pytest


class Test_Cell:
    def setup_method(self, method):
        self.__neighbours = [
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([]),
            Cell([])
        ]

        self.target = Cell(self.__neighbours)

    def test_is_dead_by_default(self):
        cell = Cell(self.__neighbours)

        assert Cell.DEAD == cell.state()

    def test_fails_with_too_many_neighbours(self):
        self.__neighbours.append(Cell([]))

        with pytest.raises(RuntimeError):
            Cell(self.__neighbours)

    def test_can_be_made_alive(self):
        self.target.make_alive()

        assert Cell.ALIVE == self.target.state()

    def test_can_be_made_dead(self):
        self.target.make_alive()

        self.target.make_dead()

        assert Cell.DEAD == self.target.state()

    def test_str_works(self):
        self.target.make_dead()
        assert "A cell which is dead" == str(self.target)

        self.target.make_alive()
        assert "A cell which is alive" == str(self.target)

    def test_becomes_alive_with_three_live_neighbours(self):
        for x in range(0, 3):
            self.__neighbours[x].make_alive()

        self.target.change_state()

        assert Cell.ALIVE == self.target.state()

    def test_stays_alive_with_two_live_neighbours(self):
        self.target.make_alive()
        self.__neighbours[0].make_alive()
        self.__neighbours[1].make_alive()

        self.target.change_state()

        assert Cell.ALIVE == self.target.state()

    def test_stays_alive_with_three_live_neighbours(self):
        self.target.make_alive()
        for x in range(0, 3):
            self.__neighbours[x].make_alive()

        self.target.change_state()

        assert Cell.ALIVE == self.target.state()

    def test_starves_with_one_live_neighbour(self):
        self.target.make_alive()
        self.__neighbours[0].make_alive()

        self.target.change_state()

        assert Cell.DEAD == self.target.state()

    def test_starves_with_no_live_neighbours(self):
        self.target.make_alive()

        self.target.change_state()

        assert Cell.DEAD == self.target.state()

    @pytest.mark.parametrize('cell_indices', [
        ([0, 1, 2, 3]),
        ([1, 2, 4, 6, 7]),
    ])
    def test_is_crowded_out_with_four_or_more_live_neighbours(self, cell_indices):
        self.make_neighbours_alive(cell_indices)

        self.target.change_state()

        assert Cell.DEAD == self.target.state()

    @pytest.mark.parametrize('cell_indices', [
        ([0, 1]),
        ([1, 2, 4, 6]),
        ([]),
    ])
    def test_stays_dead_if_three_neighbours_are_not_alive(self, cell_indices):
        self.make_neighbours_alive(cell_indices)

        self.target.change_state()

        assert Cell.DEAD == self.target.state()

    def make_neighbours_alive(self, cell_indices):
        for index in cell_indices:
            self.__neighbours[index].make_alive()
