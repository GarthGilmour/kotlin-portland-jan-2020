package com.instil.coroutines

fun String.addThreadId() = "$this on thread ${Thread.currentThread().id}"
