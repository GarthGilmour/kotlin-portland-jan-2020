package com.instil.coroutines.service

interface WaldoFinder {
    suspend fun wheresWaldo(starterName: String): String
}
