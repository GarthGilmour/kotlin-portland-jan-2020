package com.instil.coroutines

import com.instil.coroutines.gui.HelloWorldApp
import tornadofx.*

fun main(args: Array<String>) {
    launch<HelloWorldApp>(args)
}
