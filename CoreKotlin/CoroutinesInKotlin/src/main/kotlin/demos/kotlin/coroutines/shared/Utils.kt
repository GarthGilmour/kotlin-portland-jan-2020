package demos.kotlin.coroutines.shared

import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.core.MediaType

fun pingSlowServer(timeout: Int): String {
    val client = ClientBuilder.newClient()
    val result = client
            .target("http://localhost:8080")
            .path("ping")
            .path(timeout.toString())
            .request(MediaType.APPLICATION_JSON)
            .get(Array<String>::class.java)

    return result?.get(0) ?: "No Response"
}

fun timeNow(): String {
    val formatter = DateTimeFormatter.ISO_TIME
    return formatter.format(LocalTime.now())
}

fun timeAndThread(item: String) = "$item at ${timeNow()} on ${Thread.currentThread().id}"
