package demos.kotlin.coroutines.methods

import demos.kotlin.coroutines.shared.pingSlowServer
import demos.kotlin.coroutines.shared.timeNow
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking


fun main() = runBlocking(Dispatchers.IO) {
    fun pingSlowServerAsync(timeout: Int) = async { pingSlowServer(timeout) }

    println("Program starts at ${timeNow()}")

    val deferredJobs = mutableListOf<Deferred<String>>()
    deferredJobs += pingSlowServerAsync(8)
    deferredJobs += pingSlowServerAsync(6)
    deferredJobs += pingSlowServerAsync(4)
    deferredJobs += pingSlowServerAsync(2)

    val results = deferredJobs
                         .map { it.await() }
                         .joinToString(
                                 prefix = "\"",
                                 postfix = "\"",
                                 separator = ", " )
    println("Results are $results")
    println("Program complete at ${timeNow()}")
}



