package imperial.currency.start

import io.kotlintest.IsolationMode
import io.kotlintest.shouldBe
import io.kotlintest.specs.ShouldSpec

class AmountSpec : ShouldSpec() {
    override fun isolationMode() = IsolationMode.InstancePerTest

    private var amount: Amount = Amount(0)

    init {
        should("handle a zero amount") {
            amount.toString() shouldBe "May God bless you"
        }
        should("handle a single penny") {
            amount.add(Unit.PENNY)
            amount.toString() shouldBe "1 penny"
        }
    }
}
