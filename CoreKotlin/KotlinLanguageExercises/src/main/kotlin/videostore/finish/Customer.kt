package videostore.finish

class Customer(val name: String) {
    val rentals = mutableListOf<Rental>()

    fun addRental(rental: Rental) = rentals.add(rental)

    fun statement(): String {
        val result = StringBuilder()
        // add header lines
        result.append("\nRental Record for $name\n")
        // add rental lines
        for (rental in rentals) {
            result.append("\t${rental.daysRented}\t${rental.movie.title}\t${rental.cost()}\n")
        }
        // add footer lines
        result.append("Amount owed is ${totalCost()}\n")
        result.append("You earned ${totalPoints()} frequent renter points\n")
        return result.toString()
    }

    private fun totalCost(): Double {
        var totalAmount = 0.0
        for (rental in rentals) {
            totalAmount += rental.cost()
        }
        return totalAmount
    }

    private fun totalPoints(): Int {
        var points = 0
        for (rental in rentals) {
            points += rental.points()
        }
        return points
    }
}