package arrow.functions.composition

import arrow.syntax.function.*
import java.io.BufferedReader
import java.io.FileReader
import kotlin.streams.toList

val source = { name: String -> "data/$name" }

val allLines = { path: String ->
    val reader = BufferedReader(FileReader(path))
    reader.use {
        it.lines().toList()
    }
}

val findMatches = { input: List<String> ->
    val regex = "[A-Z]{2}[0-9]{2}".toRegex()
    input.filter(regex::matches)
}

fun main() {
    val composedFunc1 = findMatches compose allLines compose source
    println(composedFunc1("grepInput.txt"))

    val composedFunc2 = source forwardCompose allLines forwardCompose findMatches
    println(composedFunc2("grepInput.txt"))

    val composedFunc3 = source andThen allLines andThen findMatches
    println(composedFunc3("grepInput.txt"))
}
