package arrow.functions.partial.application.basic

import arrow.syntax.function.*

fun main() {
    demo1()
    printLine()
    demo2()
    printLine()
    demo3()
}

fun printLine() = println("-------------")

fun demo1() {
    val addNums = { no1: Int, no2: Int ->
        println("Adding $no1 to $no2")
        no1 + no2
    }
    val addSeven = addNums.partially2(7)
    val result = addSeven(3)
    println(result)
}

fun demo2() {
    val addNums = { no1: Int, no2: Int ->
        println("Adding $no1 to $no2")
        no1 + no2
    }
    val addSeven = addNums.partially2(7)
    val addSevenToThree = addSeven.partially1(3)
    val result = addSevenToThree()
    println(result)
}

fun demo3() {
    val addNums = { no1: Int, no2: Int ->
        println("Adding $no1 to $no2")
        no1 + no2
    }
    val addSeven = addNums.reverse().partially2(7)
    val result = addSeven(3)
    println(result)
}
