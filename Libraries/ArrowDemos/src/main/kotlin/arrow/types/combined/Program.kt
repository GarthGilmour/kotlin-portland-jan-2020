package arrow.types.combined

import arrow.core.Try
import arrow.core.Tuple4
import arrow.core.extensions.`try`.monad.binding
import arrow.data.Invalid
import arrow.data.Valid
import arrow.data.Validated
import arrow.data.extensions.validated.applicative.applicative
import arrow.data.fix
import arrow.typeclasses.Semigroup

class Employee(val id: String, val age: Int, val dept: String, val salary: Double) {
    constructor(t: Tuple4<String, Int, String, Double>) : this(t.a, t.b, t.c, t.d)

    override fun toString() = "$id of age $age working in $dept earning $salary"
}

fun askQuestion(question: String): String {
    println(question)
    return readLine() ?: ""
}

fun checkID(): Try<Validated<String, String>> {
    val regex = Regex("[A-Z]{2}[0-9]{2}")
    val response = askQuestion("Whats your ID?")
    return Try {
        if (regex.matches(response)) Valid(response)
        else Invalid("Bad ID")
    }
}

fun checkAge(): Try<Validated<String, Int>> {
    val response = Try { askQuestion("How old are you?").toInt() }
    return response.map { num ->
        if (num > 16) Valid(num)
        else Invalid("Bad Age")
    }
}

fun checkSalary(): Try<Validated<String, Double>> {
    val response = Try { askQuestion("What is your salary?").toDouble() }
    return response.map { num ->
        if (num > 15000.0) Valid(num)
        else Invalid("Bad Salary")
    }
}

fun checkDept(): Try<Validated<String, String>> {
    val depts = listOf("HR", "Sales", "IT")
    val response = askQuestion("Where do you work?")
    return Try {
        if (depts.contains(response)) Valid(response)
        else Invalid("Bad Dept")
    }
}

fun success(emp: Employee) = println("Created $emp")
fun exception(ex: Throwable) = println("Exception occurred - ${ex.message}")
fun invalid(msg: String?) = println("Validation error occurred - $msg")

fun main() {
    val app = Validated.applicative(object : Semigroup<String> {
        override fun String.combine(b: String) = "$this $b"
    })

    binding {
        val id = checkID().bind()
        val age = checkAge().bind()
        val dept = checkDept().bind()
        val salary = checkSalary().bind()
        app.map(id, age, dept, salary, ::Employee).fix()
    }.fold(
            { exception(it) },
            { it.fold(::invalid, ::success) })

}
