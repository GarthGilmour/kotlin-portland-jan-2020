package arrow.types.trying.monadic

import arrow.core.Try
import java.io.BufferedReader
import java.io.FileReader
import arrow.core.extensions.`try`.monad.binding

fun firstLine(path: String): Try<String> {
    fun readFirstLine(path: String): String {
        val reader = BufferedReader(FileReader(path))
        return reader.use { it.readLine() }
    }
    return Try { readFirstLine(path) }
}

fun fullPath(str: String) = "data/$str"

fun readFromFiles(input: String): String? {
    val result = binding {
        val one = firstLine(fullPath(input)).bind()
        val two = firstLine(fullPath(one)).bind()
        val three = firstLine(fullPath(two)).bind()
        val four = firstLine(fullPath(three)).bind()
        firstLine(fullPath(four)).bind()
    }
    return result.fold({ it.message }, { it })
}

fun main() {
    println("Path navigation produced '${readFromFiles("input.txt")}'")
    println("Path navigation produced '${readFromFiles("foobar")}'")
}
