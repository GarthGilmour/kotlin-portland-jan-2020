package arrow.types.trying.basic

import arrow.core.*
import java.io.BufferedReader
import java.io.FileReader

fun firstLine(path: String): Try<String> {
    fun readFirstLine(path: String): String {
        val reader = BufferedReader(FileReader(path))
        return reader.use { it.readLine() }
    }
    return Try { readFirstLine(path) }
}

fun print1(input: Try<String>): Unit {
    when(input) {
        is Success -> println("Read '${input.value}'")
        is Failure -> println("Threw '${input.exception.message}'")
    }
}

fun print2(input: Try<String>): Unit {
    val result = input.fold({ "Threw '${it.message}'" }, { "Read '$it'" })
    println(result)
}

fun print3(input: Try<String>): Unit {
    input.map { println("Read '$it'") }
}

fun print4(input: String) {
    fun fullPath(str: String) = "data/$str"

    val finalResult = firstLine(fullPath(input)).flatMap { one ->
        firstLine(fullPath(one)).flatMap { two ->
            firstLine(fullPath(two)).flatMap { three ->
                firstLine(fullPath(three)).flatMap { four ->
                    firstLine(fullPath(four)).map { result ->
                        result
                    }
                }
            }
        }
    }
    val message = finalResult.fold({ it.message }, { it })
    println("Path navigation produced '$message'")
}

fun printLine() = println("---------------")

fun main() {
    print1(firstLine("data/input4.txt"))
    print1(firstLine("foobar.txt"))
    printLine()
    print2(firstLine("data/input4.txt"))
    print2(firstLine("foobar.txt"))
    printLine()
    print3(firstLine("data/input4.txt"))
    print3(firstLine("foobar.txt"))
    printLine()
    print4("input.txt")
    print4("foobar.txt")
}
