package arrow.types.either

import arrow.core.*
import io.kotlintest.specs.ShouldSpec
import io.kotlintest.assertions.arrow.either.*

fun checkNum(number:Int) : Either<Int, Int> {
    return if(number % 2 == 0) Right(number) else Left(number)
}

class EitherTest : ShouldSpec() {

    init {
        should("be able to detect Right") {
            checkNum(4).shouldBeRight(4)
        }
        should("be able to detect Left") {
            checkNum(5).shouldBeLeft(5)
        }
    }
}