package arrow.lenses

import arrow.optics.optics

@optics
data class Postcode(val value: String) {
    companion object {}
    override fun toString() = "$value"
}

@optics
data class Address(val street: String, val postcode: Postcode) {
    companion object {}
    override fun toString() = "$street ($postcode)"
}

@optics
data class Person(val name: String, val address: Address) {
    companion object {}
    override fun toString() = "$name living at $address"
}

fun main() {
    val oldPerson = Person("Dave", Address("10 Arcatia Road", Postcode("BT26 ABC")))
    println(oldPerson)

    val personAddressPostCode = Person.address compose Address.postcode compose Postcode.value
    val newPerson = personAddressPostCode.modify(oldPerson) { _ -> "BT37 DEF" }
    println(newPerson)
}
